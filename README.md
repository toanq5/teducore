# ASP.NET Core 2.1
# Technologies:
- ASP.NET Core 2.1
- Entity Framework Core 2.1
- Dapper
- SQL Server 2012
- AutoMapper
- Domain Drivent Design
- ASP.NET Core Identity 2.1
- JWT Token
- Jquery
# Patterns:
- Repository 
- Unit Of Work
- DI

